from rest_framework.routers import DefaultRouter
from .views import (ServiceViewSet,
                    UserViewSet,
                    NoteViewSet,
                    ClientViewSet,
                    PlaceViewSet,)


router = DefaultRouter()
router.register('services', ServiceViewSet)
router.register('users', UserViewSet)
router.register('notes', NoteViewSet)
router.register('clients', ClientViewSet)
router.register('places', PlaceViewSet)
