from rest_framework import serializers
from django.contrib.auth.models import User
from .models import (Service,
                     Notes,
                     Place,
                     Client)


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)    

    class Meta:
        model = User
        fields = ('id', 'url', 'password', 'username', 'email', 'groups')


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class NoteSerializer(serializers.ModelSerializer):
    user_created = UserSerializer(read_only=True)

    class Meta:
        model = Notes
        fields = '__all__'


class ServiceSerializer(serializers.ModelSerializer):
    user_created = UserSerializer(read_only=True)
    professional = UserSerializer(read_only=True)
    notes = NoteSerializer(many=True, read_only=True)

    class Meta:
        model = Service
        fields = '__all__'
