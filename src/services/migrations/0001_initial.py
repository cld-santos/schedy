# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-14 02:11
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('alias', models.CharField(max_length=50)),
                ('e_mail', models.EmailField(max_length=254)),
                ('birth_date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Notes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField(max_length=500)),
                ('status', models.CharField(choices=[('ACTIVE', 'Active'), ('PENDING', 'Pending'), ('DONE', 'Done'), ('CANCELED', 'Canceled')], default='ACTIVE', max_length=8, verbose_name='Status')),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateField()),
                ('description', models.TextField(max_length=500)),
                ('title', models.CharField(max_length=100)),
                ('status', models.CharField(choices=[('ACTIVE', 'Active'), ('PENDING', 'Pending'), ('DONE', 'Done'), ('CANCELED', 'Canceled')], default='ACTIVE', max_length=8, verbose_name='Status')),
                ('professional', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='professional_services', to=settings.AUTH_USER_MODEL)),
                ('user_created', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='created_services', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='notes',
            name='place',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notes', to='services.Place'),
        ),
        migrations.AddField(
            model_name='notes',
            name='service',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notes', to='services.Service'),
        ),
        migrations.AddField(
            model_name='notes',
            name='user_created',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='created_notes', to=settings.AUTH_USER_MODEL),
        ),
    ]
