from django.db import models
from django.contrib.auth.models import User
from localflavor.br.forms import BRCPFField, BRCNPJField
from django.utils.translation import ugettext_lazy as _

STATUS_ACTIVE = "ACTIVE"
STATUS_PENDING = "PENDING"
STATUS_DONE = "DONE"
STATUS_CANCELED = "CANCELED"
STATUS_CHOICES = (
    (STATUS_ACTIVE, _('Active')),
    (STATUS_PENDING, _('Pending')),
    (STATUS_DONE, _('Done')),
    (STATUS_CANCELED, _('Canceled')),
)


class Place(models.Model):
    name = models.CharField(max_length=100)


class Client(models.Model):
    name = models.CharField(max_length=50)
    alias = models.CharField(max_length=50)
    e_mail = models.EmailField()
    cpf = BRCPFField()
    cnpj = BRCNPJField()
    birth_date = models.DateField()
    address = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=20)


class Service(models.Model):
    user_created = models.ForeignKey(User, related_name="created_services")

    client = models.ForeignKey(Client, related_name="client")
    professional = models.ForeignKey(
        User,
        related_name="professional_services"
    )
    date_created = models.DateField()

    description = models.TextField(max_length=500)
    title = models.CharField(max_length=100)
    status = models.CharField(max_length=8,
                              verbose_name=_('Status'),
                              choices=STATUS_CHOICES,
                              default=STATUS_ACTIVE)


class Notes(models.Model):
    user_created = models.ForeignKey(User, related_name="created_notes")
    date_created = models.DateField()

    place = models.ForeignKey(Place, related_name="notes", null=True)
    service = models.ForeignKey(Service, related_name="notes")
    description = models.TextField(max_length=500)
    status = models.CharField(max_length=8,
                              verbose_name=_('Status'),
                              choices=STATUS_CHOICES,
                              default=STATUS_ACTIVE)

    class Meta:
        ordering = ['-date_created']
