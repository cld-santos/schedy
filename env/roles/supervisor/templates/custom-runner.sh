#!/bin/bash

NAME= {{ app_name }}

VIRTUALENV_BASE_DIR={{ venv_folder }}
VIRTUALENV_DIR=$VIRTUALENV_BASE_DIR/bin

APP_BASE_DIR={{ app_folder }}
DJANGO_DIR=$APP_BASE_DIR/src

# configure the sock file. create the directory if necessary
SOCK_FILE=$APP_BASE_DIR/gunicorn.sock
RUN_DIR=$(dirname $SOCK_FILE)
test -d $RUN_DIR || mkdir -p $RUN_DIR

# configure the log file. create the directory if necessary
LOG_FILE=$APP_BASE_DIR/logs/supervisor-$NAME.log
LOG_DIR=$(dirname $LOG_FILE)
test -d $LOG_DIR || mkdir -p $LOG_DIR

# user settings
USER='{{ app_user }}'
GROUP='{{ app_user }}'

NUM_WORKERS=4

cd $VIRTUALENV_DIR
source activate
cd $DJANGO_DIR

export PYTHONPATH=$DJANGO_DIR:$PYTHONPATH

exec gunicorn {{ app_name }}.wsgi:application \
   --name {{ app_name }} \
   --workers $NUM_WORKERS \
   --user=$USER --group=$GROUP \
   --log-level=debug \
   --bind=127.0.0.1:8000 \
   --timeout 500
